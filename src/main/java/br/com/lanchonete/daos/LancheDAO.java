package br.com.lanchonete.daos;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.com.lanchonete.models.Lanche;
import br.com.lanchonete.models.TipoPreco;

@Repository
@Transactional
public class LancheDAO {

	@PersistenceContext
	private EntityManager manager;
	
	public void gravar(Lanche lanche) {
		manager.persist(lanche);
	}

	public List<Lanche> listar() {
		return manager.createQuery("select distinct(p) from Lanche p", 
				Lanche.class).getResultList();
	}
//
//	public Lanche find(Integer id) {
//		return manager.createQuery("select distinct(p) from Lanche p "
//				+ " join fetch p.precos preco where p.id = :id", Lanche.class)
//				.setParameter("id",	id)
//				.getSingleResult();
//	}
//	
//	public BigDecimal somaPrecosPorTipo(TipoPreco tipoPreco) {
//		TypedQuery<BigDecimal> query = manager
//			.createQuery("select sum(preco.valor) from Lanche p "
//				+ " join p.precos preco where preco.tipo = :tipoPreco", BigDecimal.class);
//		query.setParameter("tipoPreco", tipoPreco);
//		
//		return query.getSingleResult();
//	}
	
}










