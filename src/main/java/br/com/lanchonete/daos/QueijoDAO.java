package br.com.lanchonete.daos;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.com.lanchonete.models.Queijo;

@Repository
@Transactional
public class QueijoDAO {

	@PersistenceContext
	private EntityManager manager;
	
	public void gravar(Queijo queijo) {
		manager.persist(queijo);
	}

	public List<Queijo> listar() {
		return manager.createQuery("select distinct(q) from Queijo q order by q.titulo", 
				Queijo.class).getResultList();
	}

//	public Queijo find(Integer id) {
//		return manager.createQuery("select distinct(p) from Queijo q "
//				+ " join fetch p.precos preco where p.id = :id", Queijo.class)
//				.setParameter("id",	id)
//				.getSingleResult();
//	}
	
//	public BigDecimal somaPrecosPorTipo(TipoPreco tipoPreco) {
//		TypedQuery<BigDecimal> query = manager
//			.createQuery("select sum(preco.valor) from Queijo p "
//				+ " join p.precos preco where preco.tipo = :tipoPreco", BigDecimal.class);
//		query.setParameter("tipoPreco", tipoPreco);
//		
//		return query.getSingleResult();
//	}
	
}










