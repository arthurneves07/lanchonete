package br.com.lanchonete.daos;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.com.lanchonete.models.TipoPao;

@Repository
@Transactional
public class TipoPaoDAO {

	@PersistenceContext
	private EntityManager manager;
	
	public void gravar(TipoPao tipoPao) {
		manager.persist(tipoPao);
	}

	public List<TipoPao> listar() {
		return manager.createQuery("select distinct(tp) from TipoPao tp order by tp.titulo", 
				TipoPao.class).getResultList();
	}

//	public TipoPao find(Integer id) {
//		return manager.createQuery("select distinct(p) from TipoPao q "
//				+ " join fetch p.precos preco where p.id = :id", TipoPao.class)
//				.setParameter("id",	id)
//				.getSingleResult();
//	}
	
//	public BigDecimal somaPrecosPorTipo(TipoPreco tipoPreco) {
//		TypedQuery<BigDecimal> query = manager
//			.createQuery("select sum(preco.valor) from TipoPao p "
//				+ " join p.precos preco where preco.tipo = :tipoPreco", BigDecimal.class);
//		query.setParameter("tipoPreco", tipoPreco);
//		
//		return query.getSingleResult();
//	}
	
}










