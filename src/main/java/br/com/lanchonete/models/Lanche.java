package br.com.lanchonete.models;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Lanche implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	private String titulo;

	private TipoPao tipoPao;

	private Queijo queijo;

	private Salada salada;

	private Molho molho;

	private Tempero tempero;

	private BigDecimal valor;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public TipoPao getTipoPao() {
		return tipoPao;
	}

	public void setTipoPao(TipoPao tipoPao) {
		this.tipoPao = tipoPao;
	}

	public Queijo getQueijo() {
		return queijo;
	}

	public void setQueijo(Queijo queijo) {
		this.queijo = queijo;
	}

	public Salada getSalada() {
		return salada;
	}

	public void setSalada(Salada salada) {
		this.salada = salada;
	}

	public Molho getMolho() {
		return molho;
	}

	public void setMolho(Molho molho) {
		this.molho = molho;
	}

	public Tempero getTempero() {
		return tempero;
	}

	public void setTempero(Tempero tempero) {
		this.tempero = tempero;
	}

	public BigDecimal getValor() {
		return valor;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}
	
	
	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((queijo == null) ? 0 : queijo.hashCode());
		result = prime * result + ((tipoPao == null) ? 0 : tipoPao.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Lanche other = (Lanche) obj;
		if (queijo == null) {
			if (other.queijo != null)
				return false;
		} else if (!queijo.equals(other.queijo))
			return false;
		if (tipoPao == null) {
			if (other.tipoPao != null)
				return false;
		} else if (!tipoPao.equals(other.tipoPao))
			return false;
		return true;
	}


	// public BigDecimal precoPara(TipoPreco tipoPreco) {
	// return precos.stream().filter(preco ->
	// preco.getTipo().equals(tipoPreco)).findFirst().get().getValor();
	// }

}
