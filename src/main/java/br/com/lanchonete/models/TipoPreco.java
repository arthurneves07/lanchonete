package br.com.lanchonete.models;

public enum TipoPreco {
	EBOOK, IMPRESSO, COMBO;
}
