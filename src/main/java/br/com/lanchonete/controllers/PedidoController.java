package br.com.lanchonete.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import br.com.lanchonete.daos.LancheDAO;
import br.com.lanchonete.daos.QueijoDAO;
import br.com.lanchonete.daos.TipoPaoDAO;

@Controller
public class PedidoController {

	@Autowired
	private LancheDAO lancheDao;

	@Autowired
	private QueijoDAO queijoDao;
	
	@Autowired
	private TipoPaoDAO tipoPaoDao;
	
	@RequestMapping("/")
	public ModelAndView index() {
		ModelAndView modelAndView = new ModelAndView("pedido/index");
		modelAndView.addObject("queijos", queijoDao.listar());
		modelAndView.addObject("tiposPao", tipoPaoDao.listar());
		
		return modelAndView;
	}
	
	
}








