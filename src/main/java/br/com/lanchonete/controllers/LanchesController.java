package br.com.lanchonete.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.com.lanchonete.daos.LancheDAO;
import br.com.lanchonete.daos.QueijoDAO;
import br.com.lanchonete.daos.TipoPaoDAO;
import br.com.lanchonete.models.Lanche;

@Controller
@RequestMapping("/lanches")
public class LanchesController {

	@Autowired
	private LancheDAO lancheDao;
	
	@Autowired
	private TipoPaoDAO tipoPaoDao;
	
	@Autowired
	private QueijoDAO queijoDao;
	
	
	@RequestMapping("cadastro")
	public ModelAndView cadastro(Lanche lanche) {
		ModelAndView modelAndView = new ModelAndView("lanches/cadastro");
		modelAndView.addObject("tiposPao", tipoPaoDao.listar());
		modelAndView.addObject("queijos", queijoDao.listar());
		
		return modelAndView;
	}
	
	@RequestMapping(method=RequestMethod.POST)
	public ModelAndView gravar(Lanche lanche, BindingResult result, 
			RedirectAttributes redirectAttributes) {
		
		
//		System.out.println("Queijo: " + queijoId);
		System.out.println("result: " + result);
		System.out.println("redirectAttributes: " + redirectAttributes);
		System.out.println("Erros: " + result.hasErrors());
		
		if (result.hasErrors()) {
			return cadastro(lanche);
		}
		
		
		lancheDao.gravar(lanche);
		
		redirectAttributes.addFlashAttribute("sucesso", "Lanche cadastrado com sucesso!");
		
		return new ModelAndView("redirect:lanches");
	}
	
	@RequestMapping(method=RequestMethod.GET)
	public ModelAndView listar() {
		List<Lanche> lanches = lancheDao.listar();
		ModelAndView modelAndView = new ModelAndView("lanche/lista");
		modelAndView.addObject("lanches", lanches);
		
		return modelAndView;
	}
	
//	@RequestMapping("/detalhe/{id}")
//	public ModelAndView detalhe(@PathVariable("id") Integer id) {
//		ModelAndView modelAndView = new ModelAndView("lanches/detalhe");
//		Lanche lanche = lancheDao.find(id);
//		modelAndView.addObject("Lanche", lanche);
//	
//		return modelAndView;
//	}
	
}









