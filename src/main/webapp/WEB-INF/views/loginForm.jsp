<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Login - Lucene Lanches</title>

<c:url value="/resources/css" var="cssPath" />
<link rel="stylesheet" href="${cssPath }/bootstrap.min.css">
<link rel="stylesheet" href="${cssPath }/bootstrap-theme.min.css">

<style type="text/css">
body {
	padding: 60px 0px;
}
</style>

</head>
<body>

	<div class="container">

		<div class="col-md-offset-4 col-md-4">

			<h1>Login</h1>

			<form:form servletRelativeAction="/login" method="POST">
				<div class="form-group">
					<label>E-mail</label>
					<input name="username" type="text" class="form-control" />
				</div>
				<div class="form-group">
					<label>
						<s:message code="login.senha" />
					</label>
					<input type="password" name="password" class="form-control" />
				</div>
				<button type="submit" class="btn btn-primary">Logar</button>
			</form:form>

			<p style="margin-top: 40px;">
				<small>Login: admin@lucenelanches.com</small>
				<br />
				<small><s:message code="login.senha" />: 123456</small>
			</p>
		</div>

	</div>

</body>
</html>






