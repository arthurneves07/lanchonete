<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>


<c:url value="/resources/img" var="imgPath" />
<c:url value="/resources/css" var="cssPath" />
<c:url value="/resources/js" var="jsPath" />

<link rel="stylesheet" href="${cssPath}/bootstrap.min.css">
<link rel="stylesheet" href="${cssPath}/bootstrap-theme.min.css">
<link rel="stylesheet" href="${cssPath}/style.css">

<script type="text/javascript" src="https://code.jquery.com/jquery-1.x-git.min.js"></script>


<script type="text/javascript" src="${jsPath}/bootstrap.min.js"></script>

<div class="container">

	<header id="layout-header">

		<nav class="navbar navbar-default">
			<div class="container-fluid">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed"
						data-toggle="collapse" data-target="#navbar" aria-expanded="false"
						aria-controls="navbar">
						<span class="sr-only">Toggle navigation</span> <span
							class="icon-bar"></span> <span class="icon-bar"></span> <span
							class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="#">Lucene Lanches</a>
				</div>
				<div id="navbar" class="navbar-collapse collapse">
					<ul class="nav navbar-nav">
						<li class="active"><a href="#">Home</a></li>
						<li><a href="${s:mvcUrl('PC#index').build() }"><s:message code="menu.pedido" /></a></li>
						<li><a href="#"><s:message code="menu.entrega" /></a></li>
						<li class="dropdown"><a href="#" class="dropdown-toggle"
							data-toggle="dropdown" role="button" aria-haspopup="true"
							aria-expanded="false"><s:message code="menu.idioma" /> <span class="caret"></span></a>
							<ul class="dropdown-menu">
								<li><a href="?locale=pt" rel="nofollow">
										<s:message code="menu.pt" />
								</a></li>
								<li><a href="?locale=en_US" rel="nofollow">
										<s:message code="menu.en" />
								</a></li>
							</ul></li>
					</ul>

					<ul class="nav navbar-nav navbar-right">
						<li><a href="${s:mvcUrl('LC#logout').build() }">
								<s:message code="menu.sair" />
						</a></li>
					</ul>

				</div>
			</div>
		</nav>

	</header>


</div>
