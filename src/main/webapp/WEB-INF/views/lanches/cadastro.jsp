<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Cadastro de Lanche - Lucene Lanches</title>

<c:url value="/resources/css" var="cssPath" />
<link rel="stylesheet" href="${cssPath }/bootstrap.min.css">
<link rel="stylesheet" href="${cssPath }/bootstrap-theme.min.css">

<style type="text/css">
body {
	padding: 60px 0px;
}
</style>

</head>
<body>


	<div class="container">

		<h1>Cadastro de lanche</h1>

		<form:form action="${s:mvcUrl('LC#gravar').build() }" method="POST" commandName="lanche">
			<div class="form-group">
				<label>Titulo</label>
				<form:input path="titulo" cssClass="form-control" />
				<form:errors path="titulo" />
			</div>

			<form:select path="tipoPao">
				<c:forEach items="${tiposPao}" var="tipoPao" varStatus="status">
					<form:option value="${tipoPao.id}">${tipoPao.titulo}</form:option>
				</c:forEach>
			</form:select>

			<form:select path="queijo.id">
				<c:forEach items="${queijos}" var="queijo" varStatus="status">
					<form:option value="${queijo.id}">${queijo.titulo}</form:option>
				</c:forEach>
			</form:select>


			<button type="submit" class="btn btn-primary">Cadastrar</button>
		</form:form>

	</div>

</body>
</html>






