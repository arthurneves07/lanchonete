<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="security"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="tags"%>

<tags:pageTemplate titulo="Pedido - Lucene Lanches">


	<div class="container">

		<div class="col-md-6">

			<form>

				<div class="form-group">
					<label class="full-width">
						<input type="text" name="nome" class="form-control" placeholder="<s:message code="pedido.nome" />" />
					</label>
				</div>

				<div class="form-group">
					<label class="full-width">
						<input type="text" name="endereco" class="form-control" placeholder="<s:message code="pedido.endereco" />" />
					</label>
				</div>


				<div class="form-group">
					<select name="lanche" id="lanche" class="form-control">
						<option value="" disabled selected>-- <s:message code="pedido.listaDeLanches" /> --</option>
						<option value="1">lanche1</option>
						<option value="2">lanche2</option>
						<option value="3">lanche3</option>
					</select>
				</div>



				<div class="form-group">
					<label for="tipoPao" class="col-sm-3 control-label no-padding-left"><s:message code="pedido.tipoDePao" /></label>

					<div class="col-sm-9 no-padding-right">
						<select name="tipoPao" id="tipoPao" data-name="tipoPao" class="form-control ingr" required>
							<option value="">-- <s:message code="pedido.selecione" /> --</option>
							<c:forEach items="${tiposPao}" var="tipoPao" varStatus="status">
								<option value="${tipoPao.id}">${tipoPao.titulo}</option>
							</c:forEach>
						</select>
					</div>
				</div>

				<div class="form-group">
					<label for="queijo" class="col-sm-3 control-label no-padding-left"><s:message code="pedido.queijo" /></label>

					<div class="col-sm-9 no-padding-right">
						<select name="queijo" id="queijo" data-name="queijo" class="form-control ingr" required>
							<option value="">-- <s:message code="pedido.selecione" /> --</option>
							<c:forEach items="${queijos}" var="queijo" varStatus="status">
								<option value="${queijo.id}">${queijo.titulo}</option>
							</c:forEach>
						</select>
					</div>
				</div>

				<div class="form-group">
					<label for="recheio" class="col-sm-3 control-label no-padding-left"><s:message code="pedido.recheio" /></label>

					<div class="col-sm-9 no-padding-right">
						<select name="recheio" id="recheio" data-name="recheio" class="form-control ingr" required>
							<option value="">-- <s:message code="pedido.selecione" /> --</option>
								<option value="carne">carne</option>
								<option value="frango">frango</option>
						</select>
					</div>
				</div>

				<div class="form-group">

					<label for="salada" class="col-sm-3 control-label no-padding-left strong"><s:message code="pedido.salada" /></label>

					<div class="radio col-sm-9 no-padding-right">
						<label>
							<input type="radio" name="salada" data-name="salada" value="1" class="ingr salada" />
							salada1
						</label>
						<label>
							<input type="radio" name="salada" data-name="salada" value="2" class="ingr salada" />
							salada2
						</label>
						<label>
							<input type="radio" name="salada" data-name="salada" value="3" class="ingr salada" />
							salada3
						</label>
					</div>

				</div>

				<div class="form-group">

					<label for="molho" class="col-sm-3 control-label no-padding-left strong"><s:message code="pedido.molhos" /></label>

					<div class="checkbox  col-sm-9 no-padding-right">
						<label>
							<input type="checkbox" name="molho[]" data-name="molho" value="1" class="ingr molho">
							molho1
						</label>
						<label>
							<input type="checkbox" name="molho[]" data-name="molho" value="2" class="ingr molho" />
							molho2
						</label>
						<label>
							<input type="checkbox" name="molho[]" data-name="molho" value="3" class="ingr molho" />
							molho3
						</label>
					</div>
				</div>

				<div class="form-group">

					<label for="tempero" class="col-sm-3 control-label no-padding-left strong"><s:message code="pedido.temperos" /></label>

					<div class="checkbox  col-sm-9 no-padding-right">
						<label>
							<input type="checkbox" name="tempero[]" data-name="tempero" value="1" class="ingr tempero">
							tempero1
						</label>
						<label>
							<input type="checkbox" name="tempero[]" data-name="tempero" value="2" class="ingr tempero" />
							tempero2
						</label>
						<label>
							<input type="checkbox" name="tempero[]" data-name="tempero" value="3" class="ingr tempero" />
							tempero3
						</label>
					</div>
				</div>


				<div class="form-group">
					<div class="col-md-offset-7">
						<input type="text" id="valorTotal" disabled value="0.00" class="form-control">
					</div>
				</div>

				<div class="full-width box-btn-pedido">
					<button type="button" id="btnSalvar" class="btn btn-default"><s:message code="pedido.salvar" /></button>
					<button type="button" id="btnFinalizar" class="btn btn-primary"><s:message code="pedido.finalizar" /></button>
				</div>
			</form>
		</div>


		<div class="col-md-6">

			<table id="lista-pedidos" class="table table-responsive table-striped table-hover">
				<tr class="pedido">
					<td>1 - lanch1</td>
					<td>
						<button type="button" class="btn btn-danger btn-xs"><s:message code="pedido.cancelar" /></button>
					</td>
				</tr>
				<tr class="pedido">
					<td>2 - tipoPao1 - queijo2 - salada3 - molho2 - tempero2</td>
					<td>
						<button type="button" class="btn btn-danger btn-xs"><s:message code="pedido.cancelar" /></button>
					</td>
				</tr>
			</table>


			<div class="col-md-offset-7">
				<input type="text" id="valorTotalPedido" disabled value="0.00" class="form-control">
			</div>
		</div>


	</div>


	<script>
		$('.ingr').change(function() {

			$('#lanche').val('');

			$('#valorTotal').val(0);

			calcByIngr($('#queijo').data('name'), $('#queijo').val());

			calcByIngr($('#tipoPao').data('name'), $('#tipoPao').val());

			$.each($('.salada:checked'), function(index, salada) {
				calcByIngr($(salada).data('name'), salada.value);
			});

			$.each($('.molho:checked'), function(index, molho) {
				calcByIngr($(molho).data('name'), molho.value);
			});

			$.each($('.tempero:checked'), function(index, tempero) {
				calcByIngr($(tempero).data('name'), tempero.value);
			});

		});

		$('#lanche').change(
				function() {

					var lancheId = $(this).val();

					$('#tipoPao').val('');
					$('#queijo').val('');
					$('.salada').prop('checked', false);
					$('.molho').prop('checked', false);
					$('.tempero').prop('checked', false);

					$.ajax({
						type : "POST",
						url : 'servicoCompletaLanche',
						data : {
							id : lancheId
						},
						success : function(data) {

							console.log(data);

							var vl = 0;

							$('#tipoPao').val(data.queijo.id);

							$('#queijo').val(data.queijo.id);

							$('.salada[value=' + data.salada.id + ']').prop(
									'checked', true);

							$.each(data.molho, function(index, value) {
								$('.molho[value=' + value.id + ']').prop(
										'checked', true);
								vl += value.valor;
							});

							$.each(data.tempero, function(index, value) {
								$('.tempero[value=' + value.id + ']').prop(
										'checked', true);
								vl += value.valor;
							});

							vl += data.queijo.valor;
							vl += data.salada.valor;

							$('#valorTotal').val(vl.toFixed(2));
						},
						error : function() {

						}
					});

				});

		function calcByIngr(name, id) {

			$.ajax({
				type : "POST",
				url : 'servicoGetPrecoIngr',
				data : {
					name : name,
					id : id
				},
				success : function(data) {
					console.log(data);
					var vl = parseFloat($('#valorTotal').val())
							+ parseFloat(data.valor.toFixed(2));

					$('#valorTotal').val(vl.toFixed(2));

				},
			});
		}

		$('#btnSalvar')
				.click(
						function() {

							var qtdAtualPedidos = ($('#lista-pedidos .pedido').length) + 1;
							var descPedido = qtdAtualPedidos;

							$('#tipoPao').val();

							$('#queijo').val();

							$('.salada[value=' + data.salada.id + ']').prop(
									'checked', true);

							$.each(data.molho, function(index, value) {
								$('.molho[value=' + value.id + ']').prop(
										'checked', true);
								vl += value.valor;
							});

							$.each(data.tempero, function(index, value) {
								$('.tempero[value=' + value.id + ']').prop(
										'checked', true);
								vl += value.valor;
							});

							$('#lista-pedidos').append(
									'<div class="pedido">' + qtdAtualPedidos
											+ '</div>');

						});
	</script>



</tags:pageTemplate>



















