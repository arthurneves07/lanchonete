/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50544
Source Host           : localhost:3306
Source Database       : lanchonete

Target Server Type    : MYSQL
Target Server Version : 50544
File Encoding         : 65001

Date: 2018-01-30 13:19:22
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `lanche`
-- ----------------------------
DROP TABLE IF EXISTS `lanche`;
CREATE TABLE `lanche` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `queijo` tinyblob,
  `titulo` varchar(255) DEFAULT NULL,
  `tipoPao` tinyblob,
  `molho` int(11) DEFAULT NULL,
  `salada` int(11) DEFAULT NULL,
  `tempero` int(11) DEFAULT NULL,
  `valor` decimal(19,2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of lanche
-- ----------------------------
INSERT INTO `lanche` VALUES ('1', null, 'Johnny', null, null, null, null, null);
INSERT INTO `lanche` VALUES ('2', null, 'Teste 2', null, null, null, null, null);

-- ----------------------------
-- Table structure for `lanche_queijo`
-- ----------------------------
DROP TABLE IF EXISTS `lanche_queijo`;
CREATE TABLE `lanche_queijo` (
  `Lanche_id` int(11) NOT NULL,
  `queijo_id` int(11) NOT NULL,
  UNIQUE KEY `UK_mqqpd9oqhtjefcjgw0m492i9g` (`queijo_id`),
  KEY `FK_qxvlus32lmdgq6cb4x1iw5vf` (`Lanche_id`),
  CONSTRAINT `FK_mqqpd9oqhtjefcjgw0m492i9g` FOREIGN KEY (`queijo_id`) REFERENCES `queijo` (`id`),
  CONSTRAINT `FK_qxvlus32lmdgq6cb4x1iw5vf` FOREIGN KEY (`Lanche_id`) REFERENCES `lanche` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of lanche_queijo
-- ----------------------------

-- ----------------------------
-- Table structure for `lanche_tipopao`
-- ----------------------------
DROP TABLE IF EXISTS `lanche_tipopao`;
CREATE TABLE `lanche_tipopao` (
  `Lanche_id` int(11) NOT NULL,
  `tipoPao_id` int(11) NOT NULL,
  UNIQUE KEY `UK_8f5qondlrgt3pjpolc64ryhdi` (`tipoPao_id`),
  KEY `FK_l0ufns3dw20bwdm94puk0tl5n` (`Lanche_id`),
  CONSTRAINT `FK_8f5qondlrgt3pjpolc64ryhdi` FOREIGN KEY (`tipoPao_id`) REFERENCES `tipopao` (`id`),
  CONSTRAINT `FK_l0ufns3dw20bwdm94puk0tl5n` FOREIGN KEY (`Lanche_id`) REFERENCES `lanche` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of lanche_tipopao
-- ----------------------------

-- ----------------------------
-- Table structure for `produto`
-- ----------------------------
DROP TABLE IF EXISTS `produto`;
CREATE TABLE `produto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dataLancamento` datetime DEFAULT NULL,
  `descricao` longtext,
  `paginas` int(11) NOT NULL,
  `sumarioPath` varchar(255) DEFAULT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of produto
-- ----------------------------

-- ----------------------------
-- Table structure for `produto_precos`
-- ----------------------------
DROP TABLE IF EXISTS `produto_precos`;
CREATE TABLE `produto_precos` (
  `Produto_id` int(11) NOT NULL,
  `tipo` int(11) DEFAULT NULL,
  `valor` decimal(19,2) DEFAULT NULL,
  KEY `FK_hl4xdmygc7v2x607r4rbs6x3a` (`Produto_id`),
  CONSTRAINT `FK_hl4xdmygc7v2x607r4rbs6x3a` FOREIGN KEY (`Produto_id`) REFERENCES `produto` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of produto_precos
-- ----------------------------

-- ----------------------------
-- Table structure for `queijo`
-- ----------------------------
DROP TABLE IF EXISTS `queijo`;
CREATE TABLE `queijo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) DEFAULT NULL,
  `valor` decimal(19,2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of queijo
-- ----------------------------
INSERT INTO `queijo` VALUES ('1', 'Prato', '1.00');
INSERT INTO `queijo` VALUES ('2', 'Suiço', '2.00');
INSERT INTO `queijo` VALUES ('3', 'Gouda', '2.50');

-- ----------------------------
-- Table structure for `role`
-- ----------------------------
DROP TABLE IF EXISTS `role`;
CREATE TABLE `role` (
  `nome` varchar(255) NOT NULL,
  PRIMARY KEY (`nome`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of role
-- ----------------------------
INSERT INTO `role` VALUES ('ROLE_ADMIN');

-- ----------------------------
-- Table structure for `tipopao`
-- ----------------------------
DROP TABLE IF EXISTS `tipopao`;
CREATE TABLE `tipopao` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) DEFAULT NULL,
  `valor` decimal(19,2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tipopao
-- ----------------------------
INSERT INTO `tipopao` VALUES ('1', '7 Grãos', '2.50');
INSERT INTO `tipopao` VALUES ('2', 'Integral', '2.30');
INSERT INTO `tipopao` VALUES ('3', 'Tradicional', '1.50');
INSERT INTO `tipopao` VALUES ('4', 'Australiano', '2.70');

-- ----------------------------
-- Table structure for `usuario`
-- ----------------------------
DROP TABLE IF EXISTS `usuario`;
CREATE TABLE `usuario` (
  `email` varchar(255) NOT NULL,
  `nome` varchar(255) DEFAULT NULL,
  `senha` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of usuario
-- ----------------------------
INSERT INTO `usuario` VALUES ('admin@lucenelanches.com', 'Admin', '$2a$10$lt7pS7Kxxe5JfP.vjLNSyOXP11eHgh7RoPxo5fvvbMCZkCUss2DGu');

-- ----------------------------
-- Table structure for `usuario_role`
-- ----------------------------
DROP TABLE IF EXISTS `usuario_role`;
CREATE TABLE `usuario_role` (
  `Usuario_email` varchar(255) NOT NULL,
  `roles_nome` varchar(255) NOT NULL,
  UNIQUE KEY `UK_jnmoadyberbn5oyaybe8dxskj` (`roles_nome`),
  KEY `FK_fpipx83bjblmwmw25qotdyd3` (`Usuario_email`),
  CONSTRAINT `FK_fpipx83bjblmwmw25qotdyd3` FOREIGN KEY (`Usuario_email`) REFERENCES `usuario` (`email`),
  CONSTRAINT `FK_jnmoadyberbn5oyaybe8dxskj` FOREIGN KEY (`roles_nome`) REFERENCES `role` (`nome`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of usuario_role
-- ----------------------------
INSERT INTO `usuario_role` VALUES ('admin@lucenelanches.com', 'ROLE_ADMIN');
